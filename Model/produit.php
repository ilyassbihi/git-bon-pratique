<?php

class Produit {

	private int $id;
	private string $designation;
	private float $prix_unitaire;

	public function __construct(int $id, string $designation, float $prix_unitaire){
		$this->id = $id;
		$this->designation = $designation;
		$this->prix_unitaire = $prix_unitaire;
	}

	public function get_id(): int {
		return $this->id;
	}
	public function get_designation(): string {
		return $this->designation;
	}
	public function get_prix_unitaire(): float {
		return $this->prix_unitaire;
	}

	public function set_id(int $new_id): void {
		$this->id = $new_id;
	}
	public function set_designation(int $new_designation): void {
		$this->designation = $new_designation;
	}
	public function set_prix_unitaire(int $new_prix_unitaire): void {
		$this->prix_unitaire = $new_prix_unitaire;
	}

	public function __toString(): string{
		return "id: " . $this->id . " designation: " . $this->designation . " prix: " . $this->prix_unitaire;
	}
}